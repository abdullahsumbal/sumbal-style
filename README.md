# Sumbal Basic

A Mapbox GL basemap style it is using the vector tile
schema of [OpenMapTiles](https://github.com/openmaptiles/openmaptiles). Inspired by Klokantech Basic.


## Edit the Style

Use the [Maputnik CLI](http://openmaptiles.org/docs/style/maputnik/) to edit and develop the style.
After you've started Maputnik open the editor on `localhost:8000`.

```
maputnik --watch --file style.json
```
